"""Main. Server runner and the universe entry point"""


import tornado.ioloop
import tornado.web

import preload
from cache import get_cache
from db import migrations
from handlers.apps_request_handler import RequestHandler
from handlers.auth_request_handler import GoogleAuthRequestHandler
from handlers.userapi_request_handler import UserapiRequestHandler


def make_app():
    """Returns an application entity with routes"""
    return tornado.web.Application([
        (r'/apps/(?P<app_name>[^/]+)', RequestHandler),
        (r'/auth/google', GoogleAuthRequestHandler),
        (r'/userapi', UserapiRequestHandler)
    ])


if __name__ == '__main__':
    CACHE = get_cache()
    CACHE.set('apps', preload.get_apps())

    migrations.MANAGER.ensure_latest_schema()

    APP = make_app()
    APP.listen(5000)
    tornado.ioloop.IOLoop.current().start()
