"""Application core entities for handling and processing data"""


import api_error
from api_error import APIError, APIException
from abc import ABCMeta


class AppRequestHandler(metaclass=ABCMeta):
    """
    Request handler for application
    This is generic class to be inherited by all applications
    """
    def on_incoming_request(self, data):
        """
        Processes data when handling request. Should be implemented in inheritors.

        data: incoming data
        """
        raise NotImplementedError

    def send_error(self, error=None):
        """
        Throws an error to be caught in main request handler in order
        to properly finish the API request in case of emergency.

        error: error that happened
        """
        if error is None:
            error = api_error.UNKNOWN
        elif not isinstance(error, APIError):
            error = APIError(error)

        raise APIException(error)


class AppDataValidator:
    """Data validator interface"""
    def filter_data(self, request_data, response_data):
        """
        Prepares data to be put into database. Should be implemented in inheritors.

        request_data: request data from client
        response_data: data processed by server
        """
        raise NotImplementedError


class AppManager:
    """A class that handles links to all other entities as well as some additional info."""
    REQUEST_HANDLER = AppRequestHandler

    USE_DATABASE = False
    DATA_VALIDATOR = AppDataValidator
