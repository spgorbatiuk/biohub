"""A module defining useful methods for operating app MetaData"""


from sqlalchemy import MetaData

__all__ = ['SCHEMA_HELPER']


class SchemaHelper:
    """Class for metadata updating and retrieving"""
    def __init__(self):
        self.db_md = None

    def update_metadata(self, conn):
        """
        Updates metadata with a new connection

        conn: Connection to update
        """
        self.db_md = MetaData(conn, reflect=True)

    def get_metadata(self):
        """Metadata retrieval"""
        return self.db_md


SCHEMA_HELPER = SchemaHelper()
