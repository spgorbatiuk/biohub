"""Module defining apps database schema"""


from sqlalchemy import MetaData, Table, Column, PrimaryKeyConstraint, INTEGER, TEXT, ARRAY, TIMESTAMP, Float


def get_app_metadata(table_name):
    """
    Defines table schema, adding it to the MetaData

    table_nams: a name of the table
    returns: MetaData object
    """
    app_md = MetaData()

    Table(
        table_name, app_md,
        Column('id', INTEGER, nullable=False, autoincrement=True),
        Column('user_id', INTEGER, nullable=False),
        Column('measurement_type', TEXT, nullable=False),
        Column('results', ARRAY(Float), nullable=True),
        Column('timestamp', TIMESTAMP, nullable=False),


        PrimaryKeyConstraint('id', name=f'pk_{table_name}_t_id')
    )

    return app_md
