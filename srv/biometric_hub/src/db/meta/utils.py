"""Module defining utility functions for metadata area"""


from db.meta.server_schema import SERVER_MD
from db.meta.schema_helper import SCHEMA_HELPER


def get_table(table_name):
    """
    Returns table give its name

    table_name: String name of the table
    """
    if table_name in SERVER_MD.tables:
        return SERVER_MD.tables[table_name]

    metadata = SCHEMA_HELPER.get_metadata()
    if metadata and table_name in metadata.tables:
        return metadata.tables[table_name]

    raise KeyError('Table {table_name} is not found')
