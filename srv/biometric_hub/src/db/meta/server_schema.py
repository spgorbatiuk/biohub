"""
Description of a server db
This schema does not contain info about custom apps
"""
from sqlalchemy import MetaData, Table, Column, INTEGER, VARCHAR, TIMESTAMP, func, \
    PrimaryKeyConstraint, CHAR, DATE, JSON, ForeignKeyConstraint, TEXT

import settings

__all__ = ['VERSION_TABLE_NAME', 'SERVER_MD']

VERSION_TABLE_NAME = settings.get('db.version_table_name', 'db_version')

SERVER_MD = MetaData()

Table(
    VERSION_TABLE_NAME, SERVER_MD,
    Column('version_id', INTEGER, nullable=False, autoincrement=True),
    Column('migration_id', VARCHAR(32)),                     # null if no migrations are executed
    Column('prev_migration_id', VARCHAR(32)),
    Column('created_at', TIMESTAMP, server_default=func.current_timestamp()),

    PrimaryKeyConstraint('version_id', name='pk_version_id')
)

Table(
    'user', SERVER_MD,
    Column('id', INTEGER, primary_key=True),
    Column('gender', CHAR(1), nullable=False),
    Column('birthday', DATE, nullable=False),
    Column('email', VARCHAR(64)),
    Column('name', VARCHAR(64)),
    Column('phash', VARCHAR(64)),
    Column('data', JSON),
    Column('created_at', TIMESTAMP, server_default=func.current_timestamp())
)

Table(
    'google_id_to_user_id', SERVER_MD,

    Column('id', INTEGER, primary_key=True),
    Column('google_id', TEXT, nullable=False, unique=True),
    Column('user_id', INTEGER, nullable=False),
    Column('created_at', TIMESTAMP, server_default=func.current_timestamp()),

    ForeignKeyConstraint(['user_id'], ['user.id'], ondelete='CASCADE'),
)

Table(
    'token', SERVER_MD,
    Column('id', INTEGER, primary_key=True),
    Column('value', VARCHAR(64), nullable=False, unique=True),
    Column('expires_at', TIMESTAMP),
    Column('user_id', INTEGER, nullable=False),
    Column('created_at', TIMESTAMP, server_default=func.current_timestamp()),

    ForeignKeyConstraint(['user_id'], ['user.id'], ondelete='CASCADE')
)
