"""Modules outer entities"""


from db.storager import Storager
from .connection import ConnectionManager
from .meta import utils

__all__ = ['GET_TABLE', 'GET_CONNECTION', 'STORAGER']

__CONNECTION_MANAGER = ConnectionManager()
__STORAGER = Storager()

GET_CONNECTION = __CONNECTION_MANAGER.get_connection
GET_TABLE = utils.get_table
STORAGER = __STORAGER
