"""Module providing database migration manager implementation"""


import importlib
import logging
import os
from pathlib import Path

import sqlalchemy as sa

import settings
from db import GET_CONNECTION, GET_TABLE
from db.meta.apps_schema import get_app_metadata
from db.meta.schema_helper import SCHEMA_HELPER
from db.meta.server_schema import VERSION_TABLE_NAME, SERVER_MD
from cache import get_cache


class Migration:
    """
    Migration representation
    """
    def __init__(self, module):
        self.mig_id = module.migration_id
        self.prev_id = module.prev_migration_id

        self.operation = module.op
        self.upgrade = module.upgrade

    def push(self, conn):
        """Pushes migration"""
        self.operation.bind_connection(conn)
        self.upgrade()


class MigrationManager:
    """
    Migration manager provides methods to support a database state up-to-date
    """
    @staticmethod
    def __get_migrations(lower_id=None):

        scripts_path = os.path.join(Path(__file__).parent, 'scripts')
        prev_migration_id_to_migration_module = {}

        for file_name in os.listdir(scripts_path):
            if file_name.endswith('.py'):
                spec = importlib.util.spec_from_file_location(file_name,
                                                              os.path.join(scripts_path, file_name))
                module = importlib.util.module_from_spec(spec)
                spec.loader.exec_module(module)

                if not all([module.migration_id, module.upgrade, module.op]):
                    logging.warning('MigrationManager: ignore module %s, a migration '
                                    'should have at least migration_id, upgrade'
                                    'func and op module imported', file_name)
                    continue

                prev_migration_id_to_migration_module[module.prev_migration_id] = module

        visited = {}
        ordered_migrations = []
        migration_id = None

        module = prev_migration_id_to_migration_module.get(migration_id)
        if not module:
            logging.warning('MigrationManager: no root migration found, skip all')

        while module:
            if visited.get(module.migration_id):
                raise RecursionError(f'MigrationManager: module with '
                                     f'migration_id={module.migration_id} is visited twice')

            visited[migration_id] = True
            ordered_migrations.append(Migration(module))
            migration_id = module.migration_id

            # next
            module = prev_migration_id_to_migration_module.get(migration_id)

        if not lower_id:
            return ordered_migrations

        lower_id_ix = 0
        for idx, migration in enumerate(ordered_migrations):
            if migration.mig_id == lower_id:
                lower_id_ix = idx
                break

        return ordered_migrations[lower_id_ix+1:]

    @staticmethod
    def __is_first_load(conn):
        return not conn.dialect.has_table(conn, VERSION_TABLE_NAME)

    def ensure_latest_schema(self):
        """[HN] Ensures that the latest schema is applied"""
        with GET_CONNECTION() as conn:

            db_version_t = GET_TABLE('db_version')

            first_load = self.__is_first_load(conn)

            if first_load:
                logging.warning('MigrationManager: version control table is not found, '
                                'creating all tables')
                SERVER_MD.create_all(conn)

                query = db_version_t.insert().values(migration_id=None)
                conn.execute(query)

            current_migration = None
            if not first_load:
                query = (
                    sa.select([db_version_t.c.migration_id]).
                    select_from(db_version_t).
                    order_by(db_version_t.c.created_at.desc()).
                    limit(1)
                )

                row = conn.execute(query).fetchone()
                current_migration = row['migration_id']

            migrations = self.__get_migrations(current_migration)
            if not migrations:
                logging.warning('MigrationManager: server migration is not needed, current: %s',
                                current_migration)
            else:
                for migration in migrations:
                    migration.push(conn)

                new_migration_id = migrations[-1].mig_id
                logging.warning('MigrationManager: successfully migrated to '
                                '# %d', new_migration_id)

                query = db_version_t.insert().values(migration_id=new_migration_id,
                                                     prev_migration_id=current_migration)
                conn.execute(query)

            # now check tables for applications
            self.check_apps_tables(conn)

    def check_apps_tables(self, conn):
        """[HN] Check apps tables"""
        SCHEMA_HELPER.update_metadata(conn)
        metadata = SCHEMA_HELPER.get_metadata()

        cache = get_cache()
        apps = cache.get('apps')

        apps_table_prefix = settings.get('apps_table_prefix')

        table_created = False
        for app_name, app_params in apps.items():
            table_name = f'{apps_table_prefix}{app_name}'
            if app_params['manager'].USE_DATABASE and table_name not in metadata.tables:
                logging.warning('%s: creating tables for app = %s',
                                self.__class__.__name__, app_name)

                app_md = get_app_metadata(table_name)
                app_md.create_all(conn)
                table_created = True

        # update after pushing new tables
        if table_created:
            SCHEMA_HELPER.update_metadata(conn)
