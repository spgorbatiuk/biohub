"""File defining all module's entities available"""


from db.migrations.manager import MigrationManager

MANAGER = MigrationManager()

__all__ = [
    'MANAGER'
]
