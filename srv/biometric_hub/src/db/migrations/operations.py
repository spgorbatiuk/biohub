"""[HN] Operataions on migrations"""


from sqlalchemy import MetaData, Table

__all__ = ['OPERATIONS']


class Operations:
    """
    Migrations should be described in terms of available operations
    """
    def __init__(self):
        self.conn = None

    def bind_connection(self, conn):
        """
        Binds new connection

        conn: Connection entity
        """
        self.conn = conn

    def create_table(self, table_name, *columns):
        """
        Creates table with specified columns

        table_name: name of a new table
        columns: column names
        """
        if not self.conn:
            raise AttributeError('Operations: create table without binding a connection')

        metadata = MetaData()
        metadata.reflect(bind=self.conn)
        Table(table_name, metadata, *columns, extend_existing=True)
        metadata.create_all(self.conn)

    def drop_table(self, table_name):
        """
        Drops specified table by name

        table_name: name of the table to drop
        """
        if not self.conn:
            raise AttributeError('Operations: create table without binding a connection')

        metadata = MetaData(self.conn, reflect=True)
        table = metadata.tables.get(table_name)

        if table is None:
            raise KeyError(f'Table {table_name} is not found in a current database')

        metadata.drop_all(self.conn, [table], checkfirst=True)


# TODO: make it caps as pylint says
op = Operations()
