"""Module containing Connection manager class+"""


import logging
import sqlalchemy.exc

import settings
from db.engine import EngineManager


class ConnectionManager:
    """
    Starting point for any operations over DB.
    Connection manager creates an engine and establishes connections himself.
    """
    def __init__(self):
        self.engine_manager = EngineManager()
        self.connection_retry = 0

    def get_connection(self):
        """Returns connection to a database"""
        engine = self.engine_manager.get_engine()

        try:
            return engine.connect()
        except sqlalchemy.exc.DBAPIError as err:
            return self._on_connection_failed(err)

    def _on_connection_failed(self, error):
        """
        Fallback method to call in case of connection failure

        e: Error to raise in the case of repeating failures
        """
        self.connection_retry += 1
        logging.info('ConnectionManager: unable to connect #%s', self.connection_retry)

        if self.connection_retry > settings.get('db.connection_retries', 3):
            logging.error('ConnectionManager: max connection retry exceeded')
            raise error

        return self.get_connection()
