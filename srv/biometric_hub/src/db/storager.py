"""Module containing class carrying storage operations"""


import db
from cache import get_cache
import settings
from abc import ABCMeta


class Storager(metaclass=ABCMeta):
    """Main class for storage operations"""
    def process_data(self, app_name, request_data, response_data):
        """
        Processes and validates data, then storing it

        app_name: name of the app inquiring storaging
        request_data: request data from clint
        response_data: data processed by the particular app
        """
        cache = get_cache()
        app_cache = cache.get('apps').get(app_name)
        app_data_validator = app_cache['dataValidator']

        filtered_data = app_data_validator.filter_data(request_data, response_data)
        if filtered_data:
            self.store(app_name, filtered_data)

    def store(self, app_name, data):
        """
        Executes storaging query

        app_name: application name to determine which data table to use
        data: data itself
        """
        conn = db.GET_CONNECTION()
        table_prefix = settings.get('apps_table_prefix', 'apps_')
        table_name = f'{table_prefix}{app_name}'

        app_t = db.GET_TABLE(table_name)
        query = (app_t.insert().values(**data))
        conn.execute(query)
        conn.close()

