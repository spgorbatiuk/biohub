"""Module containing EngineManager class"""


import sqlalchemy as sa
import settings


class EngineManager:
    """[HN] Engine manager"""
    DB_TYPE_TO_DIALECT_AND_DRIVER = {
        'postgresql': 'postgresql+psycopg2'
    }

    def get_engine(self):
        """Creates new engine by url and returns it"""
        engine = sa.create_engine(self.__get_engine_uri(), encoding='utf-8')
        return engine

    def __get_engine_uri(self):
        # e.g. postgresql://scott:tiger@localhost:5432/mydatabase
        return '{dialect_and_driver}://{user}:{pswd}@{host}:{port}/{db}'.format(
            dialect_and_driver=self.DB_TYPE_TO_DIALECT_AND_DRIVER[settings.get('db.type')],
            user=settings.get('db.user'),
            pswd=settings.get('db.pass'),
            host=settings.get('db.host'),
            port=settings.get('db.port'),
            db=settings.get('db.name')
        )
