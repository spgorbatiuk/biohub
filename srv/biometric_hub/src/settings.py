"""Settings utilities and setuping"""


import json
import os


def _unpack_settings(_settings):
    _is_unpacked = False
    unpacked_settings = {}
    for key, val in _settings.items():
        if isinstance(_settings[key], dict):
            for key2, val2 in _settings[key].items():
                unpacked_settings[f'{key}.{key2}'] = val2
            _is_unpacked = True
        else:
            unpacked_settings[key] = val

    return unpacked_settings, _is_unpacked


def get(name, default=None):
    """Dict-like get function with default value a for settings"""
    val = SETTINGS.get(name)
    if val is None:
        val = default

    if val is None:
        raise ValueError

    return val


MODE = os.getenv('BIOMETRIC_HUB_MODE', 'dev')
with open(f'config/{MODE}.json', 'a+') as f:
    f.seek(0)
    DATA = f.read()
    SETTINGS = json.loads(DATA) or {}

    while True:
        SETTINGS, IS_UNPACKED = _unpack_settings(SETTINGS)
        if not IS_UNPACKED:
            break
