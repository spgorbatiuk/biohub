"""Tests for userapi endpoint request handler"""

import requests
from pytest import fixture
from api_error import *


@fixture
def userapi_url():
    yield 'http://127.0.0.1:5000/userapi'


def test_invalid_request_type(userapi_url):
    request_data = {
        'requestType': 'UNK',
        'data': {
            'some': 'data'
        }
    }

    response = requests.post(userapi_url, json=request_data, timeout=5)
    response_data = response.json()

    assert response_data['result'] == 'error' and \
           response_data['error'] == UNKNOWN_REQUEST_TYPE.error


def test_invalid_json(userapi_url):
    request_data = {
        'request': 'userByGoogleId',
        'data': {
            'some': 'data'
        }
    }

    response = requests.post(userapi_url, json=request_data, timeout=5)
    response_data = response.json()

    assert response_data['result'] == 'error' and \
           response_data['error'] == INVALID_JSON.error
