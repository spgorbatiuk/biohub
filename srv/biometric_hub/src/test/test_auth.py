"""Tests for auth module"""

import requests
from pytest import fixture

from api_error import INVALID_DATE_FORMAT, INVALID_JSON, INVALID_GOOGLE_ID_TOKEN


@fixture
def auth_url():
    """Auth endpoint"""
    yield 'http://localhost:5000/auth/google'


def test_invalid_json(auth_url):
    request_data = {
        'data': {
            'idToken': 'some_token',
            'gender': 'm'
        }
    }
    response = requests.get(auth_url, json=request_data, timeout=5)
    response_data = response.json()
    assert response_data['result'] == 'error' and response_data['error'] == INVALID_JSON.error


def test_invalid_date(auth_url):
    request_data = {
        'data': {
            'idToken': 'some_token',
            'gender': 'm',
            'birthday': '05.06.08'
        }
    }
    response = requests.get(auth_url, json=request_data, timeout=5)
    response_data = response.json()
    assert response_data['result'] == 'error' and \
           response_data['error'] == INVALID_DATE_FORMAT.error
