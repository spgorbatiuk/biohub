"""Tests related to heartrate application"""


import pytest
import requests
from apps.heartrate.request_handler import ERROR_BAD_REDSUMS, ERROR_BAD_WINSIZE, \
    ERROR_BAD_SHIFT, ERROR_INVALID_MEASURE_TYPE, ERROR_BAD_TIME


@pytest.fixture()
def root_url():
    """Apps path"""
    yield 'http://localhost:5000/apps'


@pytest.fixture()
def redsums():
    """Ordinary redsums example"""
    yield [
        232, 232, 233, 231, 234, 236, 237, 238, 238, 238, 237, 236, 235, 235, 236, 236, 236, 237,
        237, 237, 237,
        237, 238, 238, 238, 238, 238, 238, 238, 238, 238, 237, 236, 235, 235, 235, 235, 235, 234,
        234, 235, 235,
        236, 237, 237, 237, 237, 236, 236, 235, 233, 232, 233, 233, 234, 234, 235, 235, 235, 235,
        235, 235, 236,
        236, 236, 236, 236, 236, 235, 233, 232, 232, 233, 233, 234, 235, 235, 235, 235, 235, 235,
        235, 235, 235,
        235, 236, 236, 235, 233, 232, 232, 233, 234, 235, 235, 235, 235, 236, 236, 237, 237, 238,
        238, 238, 238,
        238, 237, 236, 235, 235, 236, 236, 237, 238, 238, 237, 237, 237, 238, 237, 237, 236, 235,
        234, 234, 235,
        235, 235, 235, 235, 235, 236, 237, 237, 237, 238, 238, 239, 239, 239, 238, 237, 236, 234,
        234, 233, 233,
        233, 231, 230, 230, 231, 231, 233, 233, 234, 234, 235, 236, 236, 236, 234, 232, 232, 232,
        232, 233, 234,
        233, 234, 235, 236, 238, 239, 240, 240, 240, 238, 233, 230, 230, 230, 230, 231, 232, 232,
        231, 231, 232,
        231, 229, 227, 225, 225, 222, 219, 209, 198, 193, 190, 189, 185, 184, 183, 183, 183, 183,
        183, 186, 189,
        196, 203, 212, 222, 230, 233, 237, 237, 238, 238, 238, 238, 237, 237, 236, 235, 235, 235,
        236, 237, 238,
        237, 237, 236, 236, 236, 236, 237, 237, 237, 237, 236, 236, 235, 235, 235, 235, 237, 239,
        239, 232, 217,
        200, 188, 199, 204, 210, 216, 210, 202, 194, 181, 168, 163, 164, 164, 162, 161, 151, 141,
        137, 147, 152,
        155, 173, 178, 180, 179, 173, 174, 173, 186, 206, 223, 235, 238, 238, 238, 238, 237, 238,
        238, 238, 238,
        238, 237, 237, 236, 236, 235, 233, 234, 234, 234, 234, 236, 237, 237, 237, 237, 236, 236,
        237, 238, 238,
        238, 239, 239, 239, 239, 239, 239, 238, 238, 237, 237, 238, 238, 238, 237, 236, 235, 235,
        236, 237, 237,
        238, 238, 238, 239, 238, 238, 238, 236, 235, 235, 235, 236, 237, 238, 238, 237, 237, 236,
        236, 237, 239,
        240, 240, 241, 241, 241, 241, 239, 238, 237, 237, 237, 239, 239, 239, 240, 239, 239, 239,
        239, 239, 240,
        240, 240, 240, 240, 239, 238, 238, 237, 237, 238, 239, 239, 240, 240, 240, 240, 240, 240,
        240, 240, 241,
        241, 241, 240, 239, 238, 238, 238, 239, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240,
        240, 240, 240,
        239, 239, 238, 238, 239, 239, 239, 240, 240, 240, 240, 240, 240, 240, 240, 241, 241, 241,
        240, 240, 239,
        238, 238, 239, 239, 239, 240, 240, 240, 240, 240, 240, 240, 240, 241, 241, 241, 241, 240,
        240, 239, 238,
        238, 239, 239, 240, 240, 240, 240, 240, 240, 240, 240, 240, 241, 241, 241, 241, 241, 241,
        240, 239, 238,
        238, 238, 238, 238, 239, 239, 239, 239, 239, 240, 240, 240, 240, 240, 240, 240, 239, 239,
        238, 238, 238,
        238, 239, 240, 240, 240, 240, 239, 239, 240, 240, 240, 240, 241, 240, 240, 240, 239, 238,
        238, 238, 238,
        238, 239, 239, 239, 239, 239, 239, 239, 239, 240, 240, 240, 240, 240, 240, 240, 239, 238,
        238, 237, 237,
        237, 238, 239, 239, 239, 239, 239, 239, 239, 239, 239, 239]


@pytest.fixture()
def empty_redsums():
    """Empty redsums example"""
    yield []


@pytest.fixture()
def insufficient_redsums():
    """Insufficient redsums example"""
    yield [0]*50


def test_peace_measure(root_url, redsums):
    """Plain test of peace mode measurement"""
    request_data = {
        'data': {
            'type': 'peace',
            'calculate': True,
            'store': False,
            'measure': {
                'time': 19,
                'redsums': redsums
            },
            'userId': 9999,
            'token': 'special'
        }
    }

    response = requests.get(f'{root_url}/heartrate', json=request_data, timeout=5)
    response_data = response.json()

    assert response_data['result'] == 'ok' and response_data['data']['heartrates'] == [44]


def test_relax_measure(root_url, redsums):
    """Plain test of relax mode measurement"""
    request_data = {
        'data': {
            'type': 'relax',
            'calculate': True,
            'store': False,
            'measure': {
                'time': 60,
                'windowSize': 512,
                'shift': 10,
                'redsums': redsums
            },
            'userId': 9999,
            'token': 'special'
        }
    }

    response = requests.get(f'{root_url}/heartrate', json=request_data, timeout=5)
    response_data = response.json()

    assert response_data['result'] == 'ok' and response_data['data']['heartrates'] == [51.6, 74.1]


def test_empty_redsums_data(root_url, empty_redsums):
    """Test with empty redsums data"""
    request_data = {
        'data': {
            'type': 'relax',
            'calculate': True,
            'store': False,
            'measure': {
                'time': 60,
                'windowSize': 512,
                'shift': 10,
                'redsums': empty_redsums
            },
            'userId': 9999,
            'token': 'special'
        }
    }

    response = requests.get(f'{root_url}/heartrate', json=request_data, timeout=5)
    response_data = response.json()

    assert response_data['result'] == 'error' and response_data['error'] == ERROR_BAD_REDSUMS


def test_small_redsums_data(root_url, insufficient_redsums):
    """Test with insufficient redsums data"""
    request_data = {
        'data': {
            'type': 'relax',
            'calculate': True,
            'store': False,
            'measure': {
                'time': 60,
                'windowSize': 512,
                'shift': 10,
                'redsums': insufficient_redsums
            },
            'userId': 9999,
            'token': 'special'
        }
    }

    response = requests.get(f'{root_url}/heartrate', json=request_data, timeout=5)
    response_data = response.json()

    assert response_data['result'] == 'error' and response_data['error'] == ERROR_BAD_REDSUMS


def test_bad_time(root_url, redsums):
    """Test with bad time parameter"""
    request_data = {
        'data': {
            'type': 'relax',
            'calculate': True,
            'store': False,
            'measure': {
                'time': 60000,
                'windowSize': 512,
                'shift': 10,
                'redsums': redsums
            },
            'userId': 9999,
            'token': 'special'
        }
    }

    response = requests.get(f'{root_url}/heartrate', json=request_data, timeout=5)
    response_data = response.json()

    assert response_data['result'] == 'error' and response_data['error'] == ERROR_BAD_TIME


def test_bad_winsize(root_url, redsums):
    """Test with bad window size parameter"""
    request_data = {
        'data': {
            'type': 'relax',
            'calculate': True,
            'store': False,
            'measure': {
                'time': 60,
                'windowSize': 64,
                'shift': 10,
                'redsums': redsums
            },
            'userId': 9999,
            'token': 'special'
        }
    }

    response = requests.get(f'{root_url}/heartrate', json=request_data, timeout=5)
    response_data = response.json()

    assert response_data['result'] == 'error' and response_data['error'] == ERROR_BAD_WINSIZE


def test_uneven_winsize(root_url, redsums):
    """Test with window size parameter being not power of 2"""
    request_data = {
        'data': {
            'type': 'relax',
            'calculate': True,
            'store': False,
            'measure': {
                'time': 60,
                'windowSize': 499,
                'shift': 10,
                'redsums': redsums
            },
            'userId': 9999,
            'token': 'special'
        }
    }

    response = requests.get(f'{root_url}/heartrate', json=request_data, timeout=5)
    response_data = response.json()

    assert response_data['result'] == 'error' and response_data['error'] == ERROR_BAD_WINSIZE


def test_bad_shift(root_url, redsums):
    """Test with bad window shift parameter"""
    request_data = {
        'data': {
            'type': 'relax',
            'calculate': True,
            'store': False,
            'measure': {
                'time': 60,
                'windowSize': 512,
                'shift': 0,
                'redsums': redsums
            },
            'userId': 9999,
            'token': 'special'
        }
    }

    response = requests.get(f'{root_url}/heartrate', json=request_data, timeout=5)
    response_data = response.json()

    assert response_data['result'] == 'error' and response_data['error'] == ERROR_BAD_SHIFT


def test_too_big_shift(root_url, redsums):
    """Test with too big shift parameter"""
    request_data = {
        'data': {
            'type': 'relax',
            'calculate': True,
            'store': False,
            'measure': {
                'time': 60,
                'windowSize': 512,
                'shift': 500,
                'redsums': redsums
            },
            'userId': 9999,
            'token': 'special'
        }
    }

    response = requests.get(f'{root_url}/heartrate', json=request_data, timeout=5)
    response_data = response.json()

    assert response_data['result'] == 'error' and response_data['error'] == ERROR_BAD_SHIFT


def test_bad_measure_type(root_url, redsums):
    """Test with unknown measurement mode parameter"""
    request_data = {
        'data': {
            'type': 'unknown_type',
            'calculate': True,
            'store': False,
            'measure': {
                'time': 60,
                'windowSize': 512,
                'shift': 500,
                'redsums': redsums
            },
            'userId': 9999,
            'token': 'special'
        }
    }

    response = requests.get(f'{root_url}/heartrate', json=request_data, timeout=5)
    response_data = response.json()

    assert response_data['result'] == 'error' and \
           response_data['error'] == ERROR_INVALID_MEASURE_TYPE
