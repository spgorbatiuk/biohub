import db

with db.GET_CONNECTION() as conn:
    token_t = db.GET_TABLE('token')
    user_t = db.GET_TABLE('user')

    conn.execute(user_t.insert().values(id=9999, gender='m', birthday='1982-01-12'))

    conn.execute(token_t.insert().values(value='special', user_id=9999))
