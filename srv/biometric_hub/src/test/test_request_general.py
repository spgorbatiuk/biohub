"""General test on requests and it's parameters"""


import pytest
import requests
from api_error import APP_NOT_FOUND, MALFORMED_JSON

@pytest.fixture()
def root_url():
    """URL entry point"""
    yield 'http://localhost:5000/apps'


def test_invalid_app_name(root_url):
    """Invalid application name test"""
    request_data = {
        'data': 'some_data'
    }

    response = requests.get(f'{root_url}/NOAPP', json=request_data, timeout=5)
    response_data = response.json()

    assert response_data['result'] == 'error' and response_data['error'] == APP_NOT_FOUND.error


def test_malformed_json(root_url):
    """Test with malformed json"""
    data = 'a really bad request'
    response = requests.get(f'{root_url}/heartrate', data=data, timeout=5)
    response_data = response.json()

    assert response_data['result'] == 'error' and response_data['error'] == MALFORMED_JSON.error
