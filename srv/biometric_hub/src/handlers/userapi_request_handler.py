import json
import logging
from json import JSONDecodeError
from typing import Optional, Awaitable

from tornado.web import RequestHandler
import sqlalchemy as sa

import api_error
import db
from common_utils import verify_user_token


class UserapiRequestHandler(RequestHandler):
    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    def get(self, *args, **kwargs):
        """Get request is made to be POST"""
        self.post(*args, **kwargs)

    def post(self):
        try:
            request_data = json.loads(self.request.body)
            inner_data = request_data['data']
            request_type = request_data['requestType']
        except(JSONDecodeError, TypeError, KeyError):
            self.finish(api_error.INVALID_JSON.to_dict())
            return
        except Exception as error:
            self.finish(api_error.UNKNOWN.to_dict())
            logging.error('Error happened handling auth request %s. Error: %s',
                          self.request.body, str(error))
            return

        if request_type == 'userByGoogleId':
            try:
                response_data = get_user_by_google_id(inner_data)
            except api_error.APIException as e:
                self.finish(e.api_error.to_dict())
                return
        elif request_type == 'userTests':
            try:
                user_id = inner_data['userId']
                token = inner_data['token']
                if not verify_user_token(user_id, token):
                    raise api_error.APIException(api_error.INVALID_TOKEN)
                response_data = get_user_tests_by_id(inner_data)
            except KeyError:
                self.finish(api_error.INVALID_JSON.to_dict())

            except api_error.APIException as e:
                self.finish(e.api_error.to_dict())
                return
        else:
            self.finish(api_error.UNKNOWN_REQUEST_TYPE.to_dict())
            return

        response = {
            'result': 'ok',
            'data': response_data if response_data is not None else {}
        }
        self.finish(response)


def get_user_tests_by_id(data):
    try:
        user_id = data['userId']
    except KeyError:
        raise api_error.APIException(api_error.INVALID_JSON)

    with db.GET_CONNECTION() as conn:
        heartrate_t = db.GET_TABLE('apps_heartrate')
        query = sa.select([heartrate_t]).select_from(heartrate_t).where(
            heartrate_t.c.user_id == user_id
        )
        result = conn.execute(query)
        all_tests = []
        for row in result:
            all_tests.append({
                'measurementType': row['measurement_type'],
                'results': row['results'],
                'timestamp': str(row['timestamp'])
            })
        return {
            'totalTests': len(all_tests),
            'tests': all_tests
        }


def get_user_by_google_id(data):
    try:
        google_id = data['googleId']
    except KeyError:
        raise api_error.APIException(api_error.INVALID_JSON)
    with db.GET_CONNECTION() as conn:
        google_t = db.GET_TABLE('google_id_to_user_id')
        query = sa.select([google_t]).select_from(google_t).where(
            google_t.c.google_id == google_id
        )
        result = conn.execute(query)
        row = result.fetchone()

        if not row:
            raise api_error.APIException(api_error.USER_NOT_FOUND)
        user_id = row['user_id']

        # getting user token
        token_t = db.GET_TABLE('token')
        query = sa.select([token_t]).select_from(token_t).where(
            token_t.c.user_id == user_id
        )
        result = conn.execute(query)
        row = result.fetchone()
        if not row:
            logging.error("Token not found in table for user id %d" % user_id)
            raise api_error.APIException(api_error.UNKNOWN)
        token = row['value']

        # getting user name
        user_t = db.GET_TABLE('user')
        query = sa.select([user_t]).select_from(user_t).where(
            user_t.c.id == user_id
        )
        result = conn.execute(query)
        row = result.fetchone()
        if not row:
            logging.error("User not found in table for user id %d" % user_id)
            raise api_error.APIException(api_error.UNKNOWN)
        name = row['name']

        return {
            'user_id': user_id,
            'token': token,
            'name': name
        }
