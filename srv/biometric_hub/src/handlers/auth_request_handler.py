"""Module containing authentication handlers"""


import re
import json
import logging
from json import JSONDecodeError
from typing import Optional, Awaitable
from uuid import uuid4

import tornado.web
from google.auth.transport import requests
from google.oauth2 import id_token
import sqlalchemy as sa

import api_error
import db
import settings


DATE_PATTERN = r'\A\d\d\d\d-\d\d-\d\d\Z'


class GoogleAuthRequestHandler(tornado.web.RequestHandler):
    """Google authentication handler"""

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    def get(self, *args, **kwargs):
        """Get request is made to be POST"""
        self.post(*args, **kwargs)

    def post(self):
        """Handles authentication request"""
        try:
            request_data = json.loads(self.request.body)
            inner_data = request_data['data']

            google_id_token = inner_data['idToken']
            user_gender = inner_data['gender']
            user_birthday = inner_data['birthday']  # yyyy-mm-dd
            assert re.match(pattern=DATE_PATTERN, string=user_birthday) is not None
        except (KeyError, TypeError, JSONDecodeError):
            self.finish(api_error.INVALID_JSON.to_dict())
            return
        except AssertionError:
            self.finish(api_error.INVALID_DATE_FORMAT.to_dict())
            return
        except Exception as error:
            self.finish(api_error.UNKNOWN.to_dict())
            logging.error('Error happened handling auth request %s. Error: %s',
                          self.request.body, str(error))
            return

        id_info = id_token.verify_oauth2_token(google_id_token, requests.Request(),
                                               settings.get('google_client_id'))

        if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            self.finish(api_error.INVALID_TOKEN)
            return

        google_id = id_info['sub']
        google_name = id_info['name']
        google_email = id_info['email']

        with db.GET_CONNECTION() as conn:
            google_t = db.GET_TABLE('google_id_to_user_id')

            query = sa.select([google_t]).select_from(google_t).where(
                google_t.c.google_id == google_id
            )
            result = conn.execute(query)
            row = result.fetchone()

            if row:
                user_id = row['user_id']
            else:
                user_t = db.GET_TABLE('user')

                user_data = {
                    'google': {
                        'id': google_id,
                        'email': google_email,
                        'idToken': google_id_token
                    }
                }

                result = conn.execute(
                    user_t.insert().values(
                        email=google_email,
                        name=google_name,
                        data=user_data,
                        gender=user_gender,
                        birthday=user_birthday
                    ).returning(user_t.c.id))

                row = result.fetchone()
                user_id = row['id']

                conn.execute(google_t.insert().values(google_id=google_id, user_id=user_id))

            token_t = db.GET_TABLE('token')
            token = uuid4().hex

            allowed_token_ids = sa.select([token_t.c.id]).order_by(token_t.c.created_at.desc()) \
                .limit(settings.get('max_authorized_sessions', 3) - 1)

            conn.execute(token_t.delete().where(sa.not_(token_t.c.id.in_(allowed_token_ids))))
            conn.execute(token_t.insert().values(value=token, user_id=user_id))

        self.finish({
            'result': 'ok',
            'data': {
                'user_id': user_id,
                'token': token
            }
        })
