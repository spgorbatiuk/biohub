"""
Module containing request handler for apps -- an entry point
for the request to any of the apps.
"""


import json
import logging
from json import JSONDecodeError
from typing import Optional, Awaitable

import tornado.web

import api_error
from api_error import APIException
from app_entities import AppRequestHandler
from cache import get_cache
from common_utils import verify_user_token


class RequestHandler(tornado.web.RequestHandler):
    """Handles requests and redirects them to an appropriate app"""
    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    def get(self, *args, **kwargs):
        """Get request is made to be POST"""
        self.post(*args, **kwargs)

    def post(self, app_name):
        """
        POST request handler

        app_name: name of the application to handle request. Taken from url
        """
        cache = get_cache()
        apps = cache.get('apps')
        if not apps:
            self.finish(json.dumps(api_error.APPS_NOT_LOADED.to_dict()))
            return

        app_cache = apps.get(app_name)
        if not app_cache:
            self.finish(json.dumps(api_error.APP_NOT_FOUND.to_dict()))
            return

        try:
            request_data = json.loads(self.request.body)
        except (TypeError, JSONDecodeError):
            self.finish(json.dumps(api_error.MALFORMED_JSON.to_dict()))
            return
        try:
            app_data = request_data['data']
            user_id = app_data['userId']
            token = app_data['token']
            if not verify_user_token(user_id, token):
                raise api_error.APIException(api_error.INVALID_TOKEN)
        except KeyError:
            self.finish(json.dumps(api_error.INVALID_JSON))
            return
        except api_error.APIException as error:
            self.finish(json.dumps(error.api_error.to_dict()))
            return

        request_handler: AppRequestHandler = app_cache['requestHandler']

        try:
            response_app_data = request_handler.on_incoming_request(app_data)
        except APIException as error:
            logging.error('%s: %s denied request = %s with error: %s',
                          __name__, app_name, json, str(error), exc_info=True)
            self.finish(json.dumps(error.api_error.to_dict()))
            return
        except Exception as error:
            logging.error('%s: %s denied request = %s with error: %s',
                          __name__, app_name, json, str(error), exc_info=True)
            self.finish(json.dumps(api_error.UNKNOWN.to_dict()))
            return

        response_data = {'result': 'ok'}
        if response_app_data:
            response_data['data'] = response_app_data

        self.finish(json.dumps(response_data))
