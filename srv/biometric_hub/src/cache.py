"""Caching wrapper"""


from cachelib import SimpleCache

CACHE = SimpleCache(default_timeout=0)


def get_cache():
    """Returns caching entity instance"""
    return CACHE
