import db
import sqlalchemy as sa
import api_error


def verify_user_token(user_id, token):
    with db.GET_CONNECTION() as conn:
        token_t = db.GET_TABLE('token')

        query = sa.select([token_t]).select_from(token_t).where(
            token_t.c.user_id == user_id
        )
        result = conn.execute(query)
        row = result.fetchone()

        if not row:
            raise api_error.APIException(api_error.USER_NOT_FOUND)

        existing_token = row['value']
        return token == existing_token
