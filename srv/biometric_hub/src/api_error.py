"""Exceptions and errors machinery"""


class APIError:
    """Generic error class"""
    def __init__(self, error, desc=None):
        self.error = error
        self.desc = desc

    def to_dict(self):
        """Dict transforming the information in order to further json dump it"""
        result = {
            'result': 'error',
            'error': self.error
        }

        if self.desc is not None:
            result['desc'] = self.desc

        return result


INVALID_JSON = APIError('invalid-json')
MALFORMED_JSON = APIError('json-malformed')
APP_NOT_FOUND = APIError('app-not-found')
UNKNOWN = APIError('unknown')
DATA_NOT_STORED = APIError('data-not-stored')
INVALID_TOKEN = APIError('invalid-token')
APPS_NOT_LOADED = APIError('apps-not-loaded')
UNKNOWN_REQUEST_TYPE = APIError('unknown-request-type')
USER_NOT_FOUND = APIError('user-not-found')
INVALID_DATE_FORMAT = APIError('invalid-date-format')
INVALID_GOOGLE_ID_TOKEN = APIError('invalid-google-id-token')


class APIException(Exception):
    """Custom exception to be thrown in case of emergency"""
    def __init__(self, api_error):
        super().__init__()
        self.api_error = api_error
