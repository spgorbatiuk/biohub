"""
We find existing apps and instantiate their classes (which inherite base classes)
"""

import importlib
import inspect
import logging
from pathlib import Path
import os

import settings
from app_entities import AppManager, AppDataValidator, AppRequestHandler

__all__ = ['get_apps']

APPS_TO_PARAMS = {}
APPS_LOADED = False


def _get_app_manager(app_name, app_path):
    for sub_entry in os.listdir(app_path):
        file_path = os.path.join(app_path, sub_entry)
        if os.path.isfile(file_path) and sub_entry.endswith('py'):

            module_name = sub_entry.split('.py')[0]
            module = importlib.import_module(
                f'{settings.get("path_to_apps")}.{app_name}.{module_name}'
            )

            classes = inspect.getmembers(module, inspect.isclass)
            for _, obj in classes:
                if obj is not AppManager and AppManager in inspect.getmro(obj):
                    return obj

            del module  # unimport

    return None


def _get_class_attr(obj, field, default=None):
    if hasattr(obj, field):
        return getattr(obj, field)

    return default


def _load_apps():
    apps_path = os.path.join(Path(__file__).parent, 'apps')
    for app_name in os.listdir(apps_path):

        app_path = os.path.join(apps_path, app_name)
        if os.path.isdir(app_path) and app_name != '__pycache__':

            app_manager_class = _get_app_manager(app_name, app_path)
            if app_manager_class is None:
                raise KeyError(f'An application manager was not found for {app_name}')

            request_handler_class = _get_class_attr(app_manager_class, 'REQUEST_HANDLER',
                                              default=AppRequestHandler)
            request_handler = request_handler_class()
            data_validator_class = _get_class_attr(app_manager_class, 'DATA_VALIDATOR',
                                             default=AppDataValidator)
            data_validator = data_validator_class()

            app_manager = app_manager_class()

            APPS_TO_PARAMS[app_name] = {
                'manager': app_manager,
                'requestHandler': request_handler,
                'dataValidator': data_validator
            }

    logging.warning('%s: all apps are loaded = %s', __name__, APPS_TO_PARAMS)


def get_apps():
    """Returns all loaded apps"""
    if not APPS_LOADED:
        _load_apps()

    return APPS_TO_PARAMS
