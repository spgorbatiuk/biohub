"""Heartrate application request handler"""


from apps.heartrate.calc import PeaceHeartrateCalculator, RelaxHeartrateCalculator
from db import STORAGER as Storager
from app_entities import AppRequestHandler
from api_error import APIError

INVALID_JSON = 'invalid-json'
ERROR_INVALID_MEASURE_TYPE = 'invalid-measure-type'
ERROR_BAD_TIME = 'invalid-time-value'
ERROR_BAD_REDSUMS = 'invalid-redsums-data'
ERROR_BAD_WINSIZE = 'invalid-window-size-value'
ERROR_BAD_SHIFT = 'invalid-shift-value'


class HeartrateRequestHandler(AppRequestHandler):
    """Heartrate application request handler class"""
    def process_peace_measure(self, data):
        """
        Peace mode request handler

        data: json data from request
        """
        try:
            measure = data['measure']

            time = measure['time']
            if time <= 0 or time > 10000:
                self.send_error(APIError(
                    ERROR_BAD_TIME,
                    f'Time considered to be in seconds, but value received is {time}'
                ))
                return
            redsums = measure['redsums']
            if len(redsums) < 256:
                self.send_error(APIError(
                    ERROR_BAD_REDSUMS,
                    f'Not sufficient data. Redsums array size is {len(redsums)}'
                ))
                return
        except KeyError:
            self.send_error(INVALID_JSON)
            return

        frequency = len(redsums) / time
        heartrate = PeaceHeartrateCalculator().get_heartrate(redsums, frequency)

        return {'heartrates': [heartrate]}

    def process_relax_measure(self, data):
        """
        Relax mode request handler

        data: json data from request
        """
        try:
            measure = data['measure']

            time = measure['time']
            if time <= 0 or time > 10000:
                self.send_error(APIError(
                    ERROR_BAD_TIME,
                    f'Time considered to be in seconds, but value received is {time}'
                ))
                return

            window_size = measure['windowSize']
            if window_size < 128 or window_size % 128 != 0:
                self.send_error(APIError(
                    ERROR_BAD_WINSIZE,
                    f'Bad window size value: {window_size}'
                ))
                return

            shift = measure['shift']
            if shift < 1 or shift > window_size // 2:
                self.send_error(APIError(
                    ERROR_BAD_SHIFT,
                    f'Shift value expected to be in [1, winsize/2], but got {shift}'
                ))
                return

            redsums = measure['redsums']
            if len(redsums) < 256 or len(redsums) < window_size:
                self.send_error(APIError(
                    ERROR_BAD_REDSUMS,
                    f'Not sufficient data. Redsums array size is {len(redsums)}'
                ))
                return
        except KeyError:
            self.send_error(INVALID_JSON)
            return

        frequency = len(redsums) / time
        points, exp_coefs, heartrates = \
            RelaxHeartrateCalculator().get_heartrate_data(redsums, shift, window_size, frequency)

        return {
            'heartrates': heartrates,
            'coefs': exp_coefs,
            'points': points
        }

    def on_incoming_request(self, request_data):
        calc_results = None
        if request_data.get('calculate', True):
            if request_data.get('type') == 'peace':
                calc_results = self.process_peace_measure(request_data)

            elif request_data.get('type') == 'relax':
                calc_results = self.process_relax_measure(request_data)
            else:
                self.send_error(ERROR_INVALID_MEASURE_TYPE)
                return
        if request_data.get('store', True):
            Storager.process_data(app_name='heartrate',
                                  request_data=request_data,
                                  response_data=calc_results)

        return calc_results
