"""Heartrate application manager"""

from app_entities import AppManager
from apps.heartrate.request_handler import HeartrateRequestHandler
from apps.heartrate.data_validator import HeartrateDataValidator


class HeartrateManager(AppManager):
    """Heartrate application manager class"""
    REQUEST_HANDLER = HeartrateRequestHandler

    USE_DATABASE = True
    DATA_VALIDATOR = HeartrateDataValidator
