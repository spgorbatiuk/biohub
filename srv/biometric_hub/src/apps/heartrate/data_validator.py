"""Module providing data validation tools"""

from datetime import datetime

from app_entities import AppDataValidator
from api_error import APIException, INVALID_JSON


class HeartrateDataValidator(AppDataValidator):
    """Class providing data validation for data being inserted in database"""
    def filter_data(self, request_data, response_data=None):
        try:
            user_id = request_data['userId']
        except (TypeError, KeyError):
            raise APIException(INVALID_JSON)
        data = {
            'user_id': user_id,
            'measurement_type': request_data['type'],
            'timestamp': self._to_timestamp(request_data['timestamp'])
        }
        if response_data is not None:
            data['results'] = response_data['heartrates']
        return data

    def _to_timestamp(self, val):
        if isinstance(val, int):
            date_time = datetime.fromtimestamp(val)
            return date_time.strftime('%Y-%m-%d %H:%M:%S')
        raise APIException(INVALID_JSON)
