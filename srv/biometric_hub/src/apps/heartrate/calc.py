"""Core calculation classes and methods for heart rate measurement"""
import copy

from math import sqrt
from math import log
from math import exp
from numpy.fft import fft


class HeartrateCalculator:
    """
    Primary class to perform heartrate calculations
    """
    MIN_POSSIBLE_HEARTRATE = 35
    MAX_POSSIBLE_HEARTRATE = 200

    @staticmethod
    def __interpret_fourier(interval_fft_data, interval_length, frequency):
        frequencies = []
        amplitudes = []

        for i in range(int(interval_length / 2)):
            frequencies.append(i * frequency / interval_length)
            amplitudes.append(sqrt(interval_fft_data[i].real * interval_fft_data[i].real +
                                   interval_fft_data[i].imag * interval_fft_data[i].imag))

        return frequencies, amplitudes

    def __get_clean_fourier_result(self, frequencies, amplitudes):
        bpms = []
        ampls = []
        for i, freq in enumerate(frequencies):
            if self.MIN_POSSIBLE_HEARTRATE <= freq * 60 <= self.MAX_POSSIBLE_HEARTRATE:
                bpms.append(round(freq * 60))
                ampls.append(amplitudes[i])

        return bpms, ampls

    @staticmethod
    def __get_most_probable_heartrate(bpms, amplitudes):
        max_bpm = 0
        max_amplitude = 0

        for i, bpm in enumerate(bpms):
            if amplitudes[i] > max_amplitude:
                max_bpm = bpm
                max_amplitude = amplitudes[i]

        return max_bpm

    def __process_interval(self, interval, interval_length, frequency):
        fft_data = fft(interval, n=interval_length)
        frequencies, amplitudes = self.__interpret_fourier(fft_data, interval_length, frequency)
        bpms, amplitudes = self.__get_clean_fourier_result(frequencies, amplitudes)
        return self.__get_most_probable_heartrate(bpms, amplitudes)

    @staticmethod
    def __split_into_intervals(whole_interval, intervals_count, interval_length):
        intervals = []

        for i in range(intervals_count):
            left = int(interval_length / 2 * ((i + 1) - 1))
            intervals.append(whole_interval[left:left + interval_length])

        return intervals

    def _calc_over_intervals(self, data, frequency, intervals_count, interval_length):

        if len(data) != interval_length + interval_length / 2 * (intervals_count - 1):
            raise RuntimeError(f'HeartrateCalculator: cannot split measure of length {len(data)} '
                               f'into {intervals_count} intervals of length {interval_length}')

        if interval_length % 2 != 0:
            raise RuntimeError(f'HeartrateCalculator: interval length {interval_length} '
                               f'is not power of 2')

        intervals = self.__split_into_intervals(data, intervals_count, interval_length)

        results = []
        for interval in intervals:
            results.append(self.__process_interval(interval, interval_length, frequency))

        return results

    def _calc(self, data, frequency):
        return self.__process_interval(data, len(data), frequency)


class PeaceHeartrateCalculator(HeartrateCalculator):
    """Class providing method for calculating heartrate in Peace mode"""

    MEASURE_LENGTH = 512
    INTERVALS_COUNT = 3
    INTERVAL_LENGTH = 256

    MAX_HEARTRATE_DIFF = 7  # empirical constant

    def get_heartrate(self, redsums, frequency):
        """Calculates single heart rate value using FFT and intervals method"""
        data = self.__get_prepared_data(redsums, self.MEASURE_LENGTH)

        res = self._calc(data, frequency)
        intervals_res = self._calc_over_intervals(
            data, frequency, self.INTERVALS_COUNT, self.INTERVAL_LENGTH)

        return self.__get_heartrate_if_possible(res, intervals_res)

    @staticmethod
    def __get_prepared_data(data, measure_length):
        if len(data) < measure_length:
            raise RuntimeError('Not sufficient data size')
        data = data[0:measure_length]

        return data

    def __get_heartrate_if_possible(self, whole_res, intervals_res):
        """
        Get possible heartrate using results over all data and data intervals

        :param whole_res: result when all data is used as a one interval
        :type whole_res: int
        :param intervals_res: result over intervals
        :type intervals_res: list of int
        :return: heartrate
        """

        if intervals_res[0] == intervals_res[1] == intervals_res[2]:
            return intervals_res[0]

        if intervals_res[0] == intervals_res[1]:
            common_res = intervals_res[0]
        elif intervals_res[1] == intervals_res[2]:
            common_res = intervals_res[1]
        elif intervals_res[0] == intervals_res[2]:
            common_res = intervals_res[0]
        else:
            return -1

        if whole_res - self.MAX_HEARTRATE_DIFF <= common_res <= whole_res + self.MAX_HEARTRATE_DIFF:
            return int((common_res + whole_res) / 2)

        return -1


class RelaxHeartrateCalculator(HeartrateCalculator):
    """Class providing method for calculating heartrate curve parameters in Relax mode"""
    CLEAN_HEARTRATES_MEDIAN_OFFSET = 0.35

    def get_heartrate_data(self, redsums, shift, window_size, frequency):
        """
        Infers heart rate curve estimated parameters

        redsums: An array with R channel camera data
        shift: Moving window shift on each iteration
        window_size: Moving window size
        frequency: Frames per second (FPS)
        """
        data = self.__get_prepared_data(redsums, shift, window_size)

        results = self.__process_floating_window(data, frequency, shift, window_size)

        timestamps = []
        for i in range(len(results)):
            timestamps.append((256 + i * shift) / frequency)

        results, timestamps = self.__get_clean_results(results, timestamps)
        exp_coefs = self.__get_exp_coefs(results, timestamps)

        res_0 = round(exp_coefs[0] * 10) / 10
        res_60 = round(exp_coefs[0] * exp(exp_coefs[1] * 60) * 10) / 10

        return results, exp_coefs, [res_0, res_60]

    @staticmethod
    def __get_prepared_data(data, shift, window_size):
        if len(data) < window_size:
            raise RuntimeError(
                f'RelaxHeartrateCalculator: data len {len(data)} '
                f'is less than window size {window_size}')

        data = data[(len(data) - window_size) % shift:]
        return data

    def __process_floating_window(self, data, frequency, shift, window_size):
        data_pieces_count = int((len(data) - window_size) / shift)
        heartrates = []

        for i in range(data_pieces_count):
            window = data[i * shift:i * shift + window_size]
            heartrates.append(self._calc(window, frequency))

        return heartrates

    def __get_clean_results(self, results, timestamps):
        sorted_results = copy.copy(results)
        sorted(sorted_results)

        total_results = len(results)
        mid = int(total_results / 2)

        if total_results == 0:
            median = (sorted_results[mid] + sorted_results[mid - 1]) / 2
        else:
            median = sorted_results[mid]

        clean_results = []
        ts_for_clean_results = []

        offset = self.CLEAN_HEARTRATES_MEDIAN_OFFSET

        for i, res in enumerate(results):
            if median * (1 - offset) <= res <= median * (1 + offset):
                clean_results.append(res)
                ts_for_clean_results.append(timestamps[i])

        return clean_results, ts_for_clean_results

    @staticmethod
    def __get_exp_coefs(results, timestamps):
        sum_y = .0
        sum_x2y = .0
        sum_ylny = .0
        sum_xy = .0
        sum_xylny = .0

        for i, res in enumerate(results):
            t_stamp = timestamps[i]
            val = res
            lny = log(val)
            sum_y += val
            sum_x2y += t_stamp * t_stamp * val
            sum_xylny += t_stamp * val * lny
            sum_ylny += val * lny
            sum_xy += t_stamp * val

        coef_a = (sum_x2y * sum_ylny - sum_xy * sum_xylny) / (sum_y * sum_x2y - sum_xy ** 2)
        coef_b = (sum_y * sum_xylny - sum_xy * sum_ylny) / (sum_y * sum_x2y - sum_xy ** 2)

        return exp(coef_a), coef_b
