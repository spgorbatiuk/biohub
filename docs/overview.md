# BioHub
Система для создания экспериментов по сбору биометрических данных с помощью сенсоров смартфона. Проект ориентирован на использование исследователями в области 
здоровья и физического состояния человека.

Технически система представляет собой клинетское приложение (Android), и серверную часть, на которой производится хранение, и, возможно, обработка данных.
Исследователь, желающий осуществить тот или иной эксперимент, обязан самостоятельно, или с помощью специалистов по разработке ПО, написать код, выполняющий процедуру
сбора данных на клиентском приложении, и соответствующий обработчик запросов на сервере.
Система берет на себя обязанности по общей функциональности, такой как управление аккаунтом пользователя, авторизация и аутентификация, пересылка и хранение данных.

Подробная информация о предоставляемой функциональности для разработки тестов назодится в 
[этом документе](https://gitlab.com/spgorbatiuk/biohub/-/edit/master/docs/custom_apps_creation.md).
