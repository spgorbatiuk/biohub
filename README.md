# BiometricHub

An application aggregating modules for scientific experiments on people's physical activity

## Contributing

Push to master is strongly prohibited!

Please use branch prefix like ```feature/```, ```bugfix/``` etc. when create it. 

To use commit template add section ```[commit]``` and string like ```template = ./.gitmessage``` to file .git/config

## License
[MIT](https://choosealicense.com/licenses/mit/)
