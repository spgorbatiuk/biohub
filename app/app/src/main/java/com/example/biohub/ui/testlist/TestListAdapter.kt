package com.example.biohub.ui.testlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.biohub.R


class TestListAdapter: RecyclerView.Adapter<TestListAdapter.TestListHolder>() {

    private var testData = ArrayList<TestListItem>()

    class TestListHolder(view: View) : RecyclerView.ViewHolder(view){
        private var testDate: TextView = view.findViewById(R.id.sit_test_testlist_item_date)
        private var testResult: TextView = view.findViewById(R.id.sit_test_testlist_item_result)
        private var testType: TextView = view.findViewById(R.id.sit_test_testlist_item_type)

        fun bind(item: TestListItem){
            testDate.text = item.date
            testResult.text = item.result
            testType.text = item.type
        }
    }

    fun updateItems(items: ArrayList<TestListItem>){
        testData.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestListHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_sit_test_testlist, parent, false)
        return TestListHolder(view)
    }

    override fun getItemCount() = testData.size

    override fun onBindViewHolder(holder: TestListHolder, position: Int) {
        holder.bind(testData[position])
    }

}
