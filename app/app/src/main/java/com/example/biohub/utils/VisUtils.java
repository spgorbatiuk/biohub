package com.example.biohub.utils;

import com.jjoe64.graphview.series.DataPoint;
import java.util.ArrayList;

public abstract class VisUtils {
    public static DataPoint[] getDataPoints(ArrayList<Integer> sums){
        DataPoint[] points = new DataPoint[sums.size()];
        for (int i = 0; i < sums.size(); i++){
            points[i] = new DataPoint(i, sums.get(i));
        }
        return points;
    }
}
