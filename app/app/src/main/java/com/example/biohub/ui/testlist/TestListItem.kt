package com.example.biohub.ui.testlist

data class TestListItem(var date: String, var result: String, var type: String)
