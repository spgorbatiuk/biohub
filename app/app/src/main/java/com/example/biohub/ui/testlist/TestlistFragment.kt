package com.example.biohub.ui.testlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.biohub.R
import com.example.biohub.core.Hub
import com.example.biohub.core.UserManager
import org.jetbrains.anko.support.v4.toast
import org.json.JSONObject

class TestListFragment: Fragment(){
    private val testListAdapter: TestListAdapter = TestListAdapter()
    private lateinit var viewModel: TestlistViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragmen_sit_test_testlist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val testList = view.findViewById<RecyclerView>(R.id.sit_test_testlist_scrollview)
        testList.layoutManager = LinearLayoutManager(this.context)
        testList.adapter = testListAdapter
        viewModel = ViewModelProviders.of(this).get(TestlistViewModel::class.java)
        val testData = viewModel.getData()
        testData.observe(this, Observer {
            if (it == null){
                toast("No data to show")
            }
            else {
                testListAdapter.updateItems(it)
            }
        })

    }
}