package com.example.biohub

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.biohub.core.UserManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_auth.*
import org.jetbrains.anko.MAXDPI
import org.jetbrains.anko.toast
import org.jetbrains.anko.userManager
import org.json.JSONObject

class AuthActivity: AppCompatActivity(){

    private lateinit var googleSignInClient: GoogleSignInClient
    private val _rcGoogleSignIn: Int = 14274
    private lateinit var googleButton: SignInButton
    private lateinit var userManager: UserManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        userManager = UserManager.getInstance(this)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(resources.getString(R.string.google_auth_client_id))
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)

        googleButton = findViewById(R.id.sign_in_button)
        googleButton.setSize(SignInButton.SIZE_WIDE)
        googleButton.setOnClickListener{doSignIn()}
    }


    override fun onStart() {
        super.onStart()
        val account = GoogleSignIn.getLastSignedInAccount(this)
        if (account != null) {
            handleExistingAccount(account)
        }
        else {
            googleButton.visibility = View.VISIBLE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == _rcGoogleSignIn) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun logOut() {
        googleSignInClient.signOut().addOnCompleteListener { task ->
            toast("Logged out")
        }
    }

    private fun handleSignInResult(task: Task<GoogleSignInAccount>) {
        try {
            val account = task.getResult(ApiException::class.java)
            val idToken = account!!.idToken
            val name = account.displayName
            val id = account.id
            userManager.getUserByGoogleId(id = id!!,
                successCallback = { response ->
                    val status = response.getString("result")
                    if (status.equals("ok")){
                        val data = response.getJSONObject("data")
                        userManager.setUser(data)
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                    }
                    else if (status.equals("error")){
                        val err = response.getString("error")
                        if (err.equals("user-not-found")){
                            val i = Intent(this, NewAccountActivity::class.java)
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            i.putExtra("authKind", "GOOGLE")
                            i.putExtra("idToken", idToken)
                            i.putExtra("name", name)
                            startActivity(i)
                        }
                        else {
                            Log.e("HERE", "HERE")
                            toast("Error occured: $err")
                        }
                    }
                },
                errorCallback = {
                    toast("Network error: ${it.message}")
                }
                )

        } catch (e: ApiException) {
            if (e.statusCode == 12500){
                toast(resources.getString(R.string.google_play_services_ood))
            }
            else {
                toast(resources.getString(R.string.google_auth_failed))
            }

            Log.w("AUTH", "The auth has failed with code=${e.statusCode}")
        }
    }

    private fun handleExistingAccount(account: GoogleSignInAccount) {
        userManager.getUserByGoogleId(id = account.id!!,
            successCallback = { response ->
                Log.e("AT FIRST PAGE", "received $response")
                val status = response.getString("result")
                if (status.equals("ok")){
                    val data = response.getJSONObject("data")
                    userManager.setUser(data)
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                }
                else if (status.equals("error")){
                    val err = response.getString("error")
                    if (err.equals("user-not-found")){
                        logOut()
                        recreate()
                    }
                    else {
                        Log.e("HERE", "HERE")
                        toast("Error occured: $err")
                    }
                }
            },
            errorCallback = {
                Log.e("Handlig existing", "Network err ocured")
                toast("Network error: ${it.message}")
            })
    }


    private fun doSignIn() {
        startActivityForResult(googleSignInClient.signInIntent, _rcGoogleSignIn)
    }
}