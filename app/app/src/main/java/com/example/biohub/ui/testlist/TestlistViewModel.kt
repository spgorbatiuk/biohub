package com.example.biohub.ui.testlist

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.biohub.core.Hub
import com.example.biohub.core.UserManager
import org.json.JSONObject
import org.jetbrains.anko.support.v4.toast

class TestlistViewModel(application: Application) : AndroidViewModel(application) {
    private lateinit var allTestData: MutableLiveData<ArrayList<TestListItem>>
    private lateinit var hub: Hub
    private var upToDate = false

    fun getData() : LiveData<ArrayList<TestListItem>>{
        if (!upToDate){
            loadData()
            allTestData = MutableLiveData()
            upToDate = true
        }
        return allTestData
    }

    private fun loadData() {
        hub = Hub.getInstance(getApplication())
        val user = hub.getCurrentUser()!!
        val userId = user.id
        val userToken = user.token.value
        val requestData = JSONObject()
        requestData.put("requestType", "userTests")
        val data = JSONObject()
        data.put("userId", userId)
        data.put("token", userToken)
        requestData.put("data", data)

        hub.sendData(data = requestData,
            path = "/userapi",
            successCallback = {response ->
                val status = response.getString("result")
                if (status.equals("ok")) {
                    val data = response.getJSONObject("data")
                    val allTests = data.getJSONArray("tests")
                    val totalTests = data.getInt("totalTests")
                    val testData = ArrayList<TestListItem>()

                    for (i in 0 until totalTests){
                        val test = allTests.getJSONObject(i)
                        val results = test.getJSONArray("results")
                        testData.add(TestListItem(date = test.getString("timestamp"),
                            result = "$results",
                            type = test.getString("measurementType")))
                    }
                    allTestData.postValue(testData)
                } else {
                    if (status.equals("error")){
                        Log.e("TestList ViewModel","Error: ${response.getString("error")}")
                        allTestData.postValue(null)
                    }
                }
            },
            errorCallback = {
                Log.e("TestList ViewModel", "Network error: $it")
            })

    }
}
