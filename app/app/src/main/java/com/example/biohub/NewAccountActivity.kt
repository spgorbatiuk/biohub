package com.example.biohub

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.biohub.ui.auth.NewAccountBirthdayFragment
import com.example.biohub.ui.auth.NewAccountGenderFragment
import com.google.android.material.tabs.TabLayout

class NewAccountActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_account)

        val viewPager: ViewPager = findViewById(R.id.vp_new_account)
        val tabLayout: TabLayout = findViewById(R.id.tl_new_account)

        viewPager.adapter = ViewPagerAdapter(supportFragmentManager)
        tabLayout.setupWithViewPager(viewPager)
    }

    class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        override fun getItem(position: Int): Fragment {
            return if (position == 0) {
                NewAccountGenderFragment()
            } else {
                NewAccountBirthdayFragment()
            }
        }

        override fun getCount(): Int = 2
    }
}