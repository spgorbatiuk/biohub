package com.example.biohub.core

import android.content.Context

import com.android.volley.VolleyError
import com.example.serega.biometrichub.models.User
import org.json.JSONObject


import java.util.concurrent.atomic.AtomicBoolean

class Hub private constructor(context: Context) {
    /**
     * Hub - is a main class that is also an intermediary between all module apps
     * and functionality that BiometricHub application provides. This is a singleton
     * and should be passed to every module app in order that it can use it's methods.
     */
    private val networkManager = NetworkManager(context)
    private val userManager = UserManager.getInstance(context)
    private val _tag: String = "HUB"

    companion object {
        lateinit var instance: Hub
        private var initialized = AtomicBoolean(false)
        fun getInstance(context: Context? = null): Hub{
            if (!initialized.getAndSet(true)){
                instance = Hub(context!!)
            }
            return instance
        }
    }

    fun sendData(data: JSONObject, path: String = "/", successCallback: (JSONObject) -> Unit,
                 errorCallback: ((VolleyError) -> Unit)?) {
        networkManager.makePOSTRequest(jsonParams = data,
            path = path,
            successCallback = successCallback,
            errorCallback = errorCallback)
    }

    fun getCurrentUser() : User?{
        return userManager.getUser()
    }

}
