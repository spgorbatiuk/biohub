package com.example.biohub.core

import android.content.Context
import android.util.Log
import com.android.volley.VolleyError
import com.example.serega.biometrichub.models.Token
import com.example.serega.biometrichub.models.User
import org.json.JSONObject
import java.util.concurrent.atomic.AtomicBoolean

class UserManager(
    context: Context
) {
    private var user: User? = null
    private var netWorkManager = NetworkManager(context)

    companion object {
        private lateinit var instance: UserManager
        private var initialized = AtomicBoolean(false)
        fun getInstance(context: Context? = null): UserManager {
            if (!initialized.getAndSet(true)){
                instance = UserManager(context!!)
                Log.e("User manager", "CREATION")
            }
            return instance
        }
    }

    fun setUser(user: User) {
        this.user = user
    }

    fun setUser(userData: JSONObject) {
        val id = userData.getLong("user_id")
        val name = userData.getString("name")
        val token = userData.getString("token")
        this.user = User(id, name, Token(token))
    }

    fun getUser() : User? {
        return user
    }

    fun registerGoogleUser(gender: String, birthday: String, idToken: String,
                           successCallback: (JSONObject) -> Unit, errorCallback: ((VolleyError) -> Unit)? = null) {

        val data = JSONObject()
        data.put("idToken", idToken)
        data.put("gender", gender)
        data.put("birthday", birthday)

        val jsonParams = JSONObject()
        jsonParams.put("data", data)

        netWorkManager.makePOSTRequest(path = "/auth/google", jsonParams = jsonParams,
            successCallback = successCallback, errorCallback = errorCallback)
    }

    fun getUserByGoogleId(id: String, successCallback: (JSONObject) -> Unit,
                               errorCallback: ((VolleyError) -> Unit)? = null){
        val data = JSONObject()
        data.put("googleId", id)
        val jsonParams = JSONObject()
        jsonParams.put("requestType", "userByGoogleId")
        jsonParams.put("data", data)

        netWorkManager.makePOSTRequest(path = "/userapi", jsonParams = jsonParams,
            successCallback = successCallback, errorCallback = errorCallback)
    }
}