package com.example.biohub.core

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class NetworkManager(context: Context) {
    /**
     * NetworkManager is a class providing network functionality - sending
     * and receiving requests to the server. It is intended to be exploited
     * only bu Hub, not by module apps directly.
     *
     * @param contex: Context (Activity instance) of main Activity of the application. It
     * is needed to create an instance of RequestQueue from Volley library.
     */
    companion object {
        const val SERVER_HOST = "35.228.237.91"
        const val SERVER_PORT = 5000
        const val SERVER_PROTOCOL = "http"
    }

    private val _tag = "bhub:net"
    private val requestQueue = Volley.newRequestQueue(context)

    private fun getUrl(urlParams: Map<String, String>? = null, path: String? = "/"): String {
        val baseUrl = "$SERVER_PROTOCOL://$SERVER_HOST:$SERVER_PORT$path"
        val url: String

        if (urlParams != null) {
            var params = ""
            urlParams.keys.forEach {
                params = "$params&$it=${urlParams[it]}"
            }

            url = "$baseUrl?$params"
        } else {
            url = baseUrl
        }

        return url
    }

    private fun makeRequest(urlParams: Map<String, String>? = null, jsonParams: JSONObject? = null, path: String? = "/",
                            successCallback: (JSONObject) -> Unit, errorCallback: ((VolleyError) -> Unit)? = null,
                            requestMethod: Int = Request.Method.POST) {

        val url = this.getUrl(urlParams, path)
        Log.e(_tag, url)

        val request = JsonObjectRequest(requestMethod, url, jsonParams,
            Response.Listener { response ->
                onRequestSuccess(successCallback, response)
            },
            if (errorCallback != null) Response.ErrorListener { error ->
                onRequestError(errorCallback, error)
            } else null)

        requestQueue.add(request)
    }

    private fun onRequestSuccess(successCallback: (JSONObject) -> Unit, response: JSONObject) : Unit {
        successCallback(response)
    }

    private fun onRequestError(errorCallback: ((VolleyError) -> Unit), error: VolleyError) : Unit {
        errorCallback(error)
    }


    fun makePOSTRequest(urlParams: Map<String, String>? = null, jsonParams: JSONObject? = null, path: String? = "/",
                        successCallback: (JSONObject) -> Unit, errorCallback: ((VolleyError) -> Unit)? = null) {
        makeRequest(urlParams, jsonParams, path, successCallback, errorCallback, Request.Method.POST)
    }
}