package com.example.serega.biometrichub.models

import java.time.LocalDate

data class User(
    val id: Long,
    val name: String,
    val token: Token
)