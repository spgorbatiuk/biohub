package com.example.biohub.ui.measurement

import android.Manifest
import android.content.Context
import android.graphics.Color
import android.hardware.Camera
import android.os.Bundle
import android.os.CountDownTimer
import android.os.PowerManager
import android.util.Log
import android.view.*
import android.widget.*
import androidx.core.content.PermissionChecker
import androidx.fragment.app.Fragment
import com.android.volley.NoConnectionError
import com.android.volley.VolleyError
import com.example.biohub.R
import com.example.biohub.core.Hub
import com.example.biohub.core.UserManager
import com.example.biohub.utils.ImageProcessing
import com.example.biohub.utils.VisUtils
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import org.json.JSONArray
import org.json.JSONObject
import kotlin.math.roundToInt

class MeasurementFragment: Fragment() {
    private val hub: Hub = Hub.getInstance()
    private val _tag = "SitTestMeasureFragment"
    private lateinit var preview: SurfaceView
    private lateinit var previewHolder: SurfaceHolder
    private var camera: Camera? = null
    private lateinit var timerToStart: CountDownTimer
    private lateinit var timer2goRelax: CountDownTimer
    private lateinit var timer2goPeace: CountDownTimer
    private lateinit var additionalTimer: CountDownTimer
    private lateinit var countDownField: TextView
    private lateinit var modeCaption: TextView
    private lateinit var resultTextField: TextView
    private lateinit var startButton: Button
    private lateinit var progressBar: ProgressBar
    private lateinit var radioGroup: RadioGroup
    private lateinit var winSizeSpinner: Spinner
    private lateinit var shiftSize: EditText
    private lateinit var settingsBar: LinearLayout
    private lateinit var graph: GraphView
    private lateinit var userManager: UserManager

    private var state: State = State.PreMeasure
    private val redsums = arrayListOf<Int>()
    private var timePassed = 0
    private var mode = Mode.Peace
    private lateinit var wakeLock: PowerManager.WakeLock
    private val surfaceCallback = object : SurfaceHolder.Callback {
        override fun surfaceCreated(holder: SurfaceHolder) {
            try {
                camera!!.setPreviewDisplay(previewHolder)
            } catch (t: Throwable) {
                Log.e("surfaceCallback", "Exception in setPreviewDisplay()", t)
            }

        }

        override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
            val parameters = camera!!.parameters
            val size = getSmallestPreviewSize(width, height, parameters)
            if (size != null) {
                parameters.setPreviewSize(size.width, size.height)
                Log.d(_tag, "Using width=" + size.width + " height=" + size.height)
            }
            camera!!.parameters = parameters
            camera!!.setDisplayOrientation(90)
            camera!!.startPreview()
        }

        override fun surfaceDestroyed(holder: SurfaceHolder) {
            //Doing nothing
        }
    }
    private val previewCallback = Camera.PreviewCallback { data, cam ->
        if (data == null) throw NullPointerException()
        val size = cam.parameters.previewSize ?: throw NullPointerException()

        val width = size.width
        val height = size.height

        val imgAvgs = ImageProcessing.decodeYUV420SPtoRedAvg(data.clone(), width, height)
        redsums.add(imgAvgs[0])
    }

    private fun getSmallestPreviewSize(width: Int, height: Int, parameters: Camera.Parameters): Camera.Size? {
        var result: Camera.Size? = null

        for (size in parameters.supportedPreviewSizes) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size
                } else {
                    val resultArea = result.width * result.height
                    val newArea = size.width * size.height

                    if (newArea < resultArea) result = size
                }
            }
        }

        return result
    }

    private fun initialize() {
        previewHolder.addCallback(surfaceCallback)
        val pm = this.context!!.getSystemService(Context.POWER_SERVICE) as? PowerManager
        if (pm != null){
            wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "hdr:donotdimscreen")
        }
        progressBar.max = 100

        timerToStart = object : CountDownTimer(3000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                val secsToGo = (millisUntilFinished / 1000).toFloat().roundToInt()
                countDownField.text = String.format(getString(R.string.measurment_fragment_status_countdown), secsToGo)
            }

            override fun onFinish() {
                countDownField.text = getText(R.string.sit_test_status_measurement)
                camera!!.setPreviewCallback(previewCallback)
                when (mode) {
                    Mode.Peace -> {
                        timer2goPeace.start()
                    }
                    Mode.Relax -> {
                        timer2goRelax.start()
                    }
                }
            }
        }

        timer2goRelax = object : CountDownTimer(60000, 500) {
            override fun onTick(millisUntilFinished: Long) {
                val progress = ((60000 - millisUntilFinished) / 600).toFloat().roundToInt()
                progressBar.progress = progress
            }

            override fun onFinish() {
                setAftermeasureViews()
                onMeasureFinish()
            }
        }

        timer2goPeace = object : CountDownTimer(18000, 250) {
            override fun onTick(millisUntilFinished: Long) {
                val progress = Math.round(redsums.size.toFloat() / 530 * 100)
                progressBar.progress = progress
            }

            override fun onFinish() {
                Log.e("TIMER", "finished")
                timePassed += 18
                if (redsums.size < 530) {
                    additionalTimer.start()
                } else {
                    setAftermeasureViews()
                    onMeasureFinish()
                }

            }
        }

        additionalTimer = object : CountDownTimer(1000, 250) {
            override fun onTick(millisUntilFinished: Long) {
                val progress = Math.round(redsums.size.toFloat() / 530 * 100)
                progressBar.progress = progress
            }

            override fun onFinish() {
                timePassed += 1
                if (redsums.size < 530) {
                    additionalTimer.start()
                } else {
                    setAftermeasureViews()
                    onMeasureFinish()
                }
            }
        }

        startButton.setOnClickListener {
            when (state) {
                State.PreMeasure -> {
                    setMeasureViews()
                    // auxiliaries setting
                    progressBar.progress = 0
                    redsums.clear()
                    timePassed = 0
                    // flashlight
                    val parameters = camera!!.parameters
                    parameters.flashMode = Camera.Parameters.FLASH_MODE_TORCH
                    camera!!.parameters = parameters
                    // start timer chain
                    state = State.Measure
                    timerToStart.start()
                }
                State.Measure -> {
                    setPremeasureViews()
                    cancellationMotions()
                    state = State.PreMeasure
                }
                State.AfterMeasure -> {
                    aquisitionMotions()
                    setPremeasureViews()
                    state = State.PreMeasure
                }
            }
        }

        radioGroup.setOnCheckedChangeListener { _, checkedId ->
            mode = if (checkedId == R.id.radio_peace){
                Mode.Peace
            } else {
                Mode.Relax
            }
        }
    }

    private fun setPremeasureViews(){
        startButton.text = getText(R.string.sit_test_main_button_start)
        progressBar.visibility = View.GONE
        preview.visibility = View.VISIBLE
        countDownField.visibility = View.INVISIBLE
        settingsBar.visibility = View.VISIBLE
        graph.visibility = View.GONE
    }

    private fun setMeasureViews(){
        startButton.text = getText(R.string.sit_test_main_button_stop)
        progressBar.visibility = View.VISIBLE
        countDownField.visibility = View.VISIBLE
        settingsBar.visibility = View.GONE
        graph.visibility = View.GONE
    }

    private fun setAftermeasureViews(){
        startButton.text = getText(R.string.sit_test_main_button_new_measure)
        progressBar.visibility = View.GONE
        preview.visibility = View.GONE
        countDownField.visibility = View.VISIBLE
        countDownField.text = getText(R.string.sit_test_status_sending_data)
        graph.visibility = View.VISIBLE

        startButton.isClickable = false
    }

    private fun cancellationMotions(cancelCamera: Boolean=true) {
        timerToStart.cancel()
        timer2goRelax.cancel()
        timer2goPeace.cancel()
        if (cancelCamera){
            camera!!.setPreviewCallback(null)
            val parameters = camera!!.parameters
            parameters.flashMode = Camera.Parameters.FLASH_MODE_OFF
            camera!!.parameters = parameters
        }

    }

    private fun aquisitionMotions(){
        camera = Camera.open()
    }

    private fun onMeasureFinish() {
        val parameters = camera!!.parameters
        Log.e("CAMERA", parameters.flashMode)
        parameters.flashMode = Camera.Parameters.FLASH_MODE_OFF
        camera!!.parameters = parameters
        camera!!.startPreview()
        camera!!.setPreviewCallback(null)
        previewHolder.surface.release()
        camera!!.release()
        camera = null
        state = State.AfterMeasure
        Log.e("CAMERA", "nulled")
        graph.removeAllSeries()
        val redSeries = LineGraphSeries<DataPoint>(VisUtils.getDataPoints(redsums))
        redSeries.color = Color.RED
        graph.addSeries(redSeries)

        sendData(mode)
    }

    private fun sendData(mode: Mode){
        val params = JSONObject()
        val data = JSONObject()
        val measure = JSONObject()

//        val rss = JSONArray(redsums)

        val fake_rss : IntArray = intArrayOf(232, 232, 233, 231, 234, 236, 237, 238, 238, 238, 237, 236, 235, 235, 236, 236, 236, 237,
            237, 237, 237,
            237, 238, 238, 238, 238, 238, 238, 238, 238, 238, 237, 236, 235, 235, 235, 235, 235, 234,
            234, 235, 235,
            236, 237, 237, 237, 237, 236, 236, 235, 233, 232, 233, 233, 234, 234, 235, 235, 235, 235,
            235, 235, 236,
            236, 236, 236, 236, 236, 235, 233, 232, 232, 233, 233, 234, 235, 235, 235, 235, 235, 235,
            235, 235, 235,
            235, 236, 236, 235, 233, 232, 232, 233, 234, 235, 235, 235, 235, 236, 236, 237, 237, 238,
            238, 238, 238,
            238, 237, 236, 235, 235, 236, 236, 237, 238, 238, 237, 237, 237, 238, 237, 237, 236, 235,
            234, 234, 235,
            235, 235, 235, 235, 235, 236, 237, 237, 237, 238, 238, 239, 239, 239, 238, 237, 236, 234,
            234, 233, 233,
            233, 231, 230, 230, 231, 231, 233, 233, 234, 234, 235, 236, 236, 236, 234, 232, 232, 232,
            232, 233, 234,
            233, 234, 235, 236, 238, 239, 240, 240, 240, 238, 233, 230, 230, 230, 230, 231, 232, 232,
            231, 231, 232,
            231, 229, 227, 225, 225, 222, 219, 209, 198, 193, 190, 189, 185, 184, 183, 183, 183, 183,
            183, 186, 189,
            196, 203, 212, 222, 230, 233, 237, 237, 238, 238, 238, 238, 237, 237, 236, 235, 235, 235,
            236, 237, 238,
            237, 237, 236, 236, 236, 236, 237, 237, 237, 237, 236, 236, 235, 235, 235, 235, 237, 239,
            239, 232, 217,
            200, 188, 199, 204, 210, 216, 210, 202, 194, 181, 168, 163, 164, 164, 162, 161, 151, 141,
            137, 147, 152,
            155, 173, 178, 180, 179, 173, 174, 173, 186, 206, 223, 235, 238, 238, 238, 238, 237, 238,
            238, 238, 238,
            238, 237, 237, 236, 236, 235, 233, 234, 234, 234, 234, 236, 237, 237, 237, 237, 236, 236,
            237, 238, 238,
            238, 239, 239, 239, 239, 239, 239, 238, 238, 237, 237, 238, 238, 238, 237, 236, 235, 235,
            236, 237, 237,
            238, 238, 238, 239, 238, 238, 238, 236, 235, 235, 235, 236, 237, 238, 238, 237, 237, 236,
            236, 237, 239,
            240, 240, 241, 241, 241, 241, 239, 238, 237, 237, 237, 239, 239, 239, 240, 239, 239, 239,
            239, 239, 240,
            240, 240, 240, 240, 239, 238, 238, 237, 237, 238, 239, 239, 240, 240, 240, 240, 240, 240,
            240, 240, 241,
            241, 241, 240, 239, 238, 238, 238, 239, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240,
            240, 240, 240,
            239, 239, 238, 238, 239, 239, 239, 240, 240, 240, 240, 240, 240, 240, 240, 241, 241, 241,
            240, 240, 239,
            238, 238, 239, 239, 239, 240, 240, 240, 240, 240, 240, 240, 240, 241, 241, 241, 241, 240,
            240, 239, 238,
            238, 239, 239, 240, 240, 240, 240, 240, 240, 240, 240, 240, 241, 241, 241, 241, 241, 241,
            240, 239, 238,
            238, 238, 238, 238, 239, 239, 239, 239, 239, 240, 240, 240, 240, 240, 240, 240, 239, 239,
            238, 238, 238,
            238, 239, 240, 240, 240, 240, 239, 239, 240, 240, 240, 240, 241, 240, 240, 240, 239, 238,
            238, 238, 238,
            238, 239, 239, 239, 239, 239, 239, 239, 239, 240, 240, 240, 240, 240, 240, 240, 239, 238,
            238, 237, 237,
            237, 238, 239, 239, 239, 239, 239, 239, 239, 239, 239, 239
        )
        val rss = JSONArray(fake_rss)
        val user = hub.getCurrentUser()!!
        val userId = user.id
        val token = user.token.value

        measure.put("redsums", rss)
        data.put("calc", true)
        data.put("store", true)
        data.put("userId", userId)
        data.put("token", token)

        val currentTimestamp = System.currentTimeMillis()/1000
        data.put("timestamp", currentTimestamp)

        when (mode){
            Mode.Peace -> {
                data.put("type", "peace")
                measure.put("time", 18)
            }

            Mode.Relax -> {
                data.put("type", "relax")
                measure.put("time", 60)
                when (winSizeSpinner.selectedItemPosition){
                    0 -> {
                        measure.put("windowSize", 256)
                    }
                    1 -> {
                        measure.put("windowSize", 512)
                    }
                    2 -> {
                        measure.put("windowSize", 1024)
                    }
                }
                measure.put("shift", Integer.parseInt(shiftSize.text.toString()))
            }
        }
        data.put("measure", measure)
        params.put("data", data)
        hub.sendData(data=params, path = "/apps/heartrate",
            successCallback = dataReceiveCallback, errorCallback = errorCallback)
    }

    private val errorCallback: (VolleyError) -> Unit = { err ->
        if (err is NoConnectionError){
            countDownField.text = getText(R.string.sit_test_status_no_connection)
        }
        else{
            countDownField.text = String.format(getString(R.string.measurment_fragment_status_error), err)
        }
    }

    private val dataReceiveCallback: (JSONObject) -> Unit = { resp ->
        Log.e("RECEIVED", resp.toString())

        if (resp.getString("result").equals("error")) {
            val err = resp.getString("error")
            countDownField.text = "Error: $err"
        }
        else {
            when (mode){
                Mode.Peace -> {
                    val result = resp.getJSONObject("data").getJSONArray("heartrates")[0]
                    if (result == -1){
                        countDownField.text = getText(R.string.sit_test_status_bad_measurement)
                    } else {
                        countDownField.text = String.format(getString(R.string.measurment_fragment_status_result), result)
                    }

                }
                Mode.Relax -> {
                    val results = resp.getJSONObject("data").getJSONArray("heartrates")
                    countDownField.text = String.format(getString(R.string.measurment_fragment_status_results), results[0], results[1])
                }
            }
        }



        startButton.isClickable = true
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_measurement, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        preview = view.findViewById(R.id.preview)
        previewHolder = preview.holder
        countDownField  = view.findViewById(R.id.countDownField)
        modeCaption = view.findViewById(R.id.mode_cap)
        resultTextField = view.findViewById(R.id.resultTextField)
        startButton = view.findViewById(R.id.sit_test_main_button)
        progressBar = view.findViewById(R.id.progress_bar)
        radioGroup = view.findViewById(R.id.radio_group)
        winSizeSpinner = view.findViewById(R.id.spinner)
        shiftSize = view.findViewById(R.id.shiftNum)
        settingsBar = view.findViewById(R.id.settings_bar)
        graph = view.findViewById(R.id.graph)

        userManager = UserManager.getInstance(this.context)

        initialize()
    }

    override fun onResume() {
        super.onResume()
        val permission = PermissionChecker.checkSelfPermission(this.context!!, Manifest.permission.CAMERA)
        if (permission == PermissionChecker.PERMISSION_GRANTED){
            Log.e("onResume", "permission granted")
            wakeLock.acquire(10*60*1000L /*10 minutes*/)
            if (camera == null){
                camera = Camera.open()
            }
        }
        setPremeasureViews()
    }

    override fun onPause() {
        super.onPause()
        val permission = PermissionChecker.checkSelfPermission(this.context!!, Manifest.permission.CAMERA)
        if (permission == PermissionChecker.PERMISSION_GRANTED){
            if (state == State.Measure){
                cancellationMotions()
                state = State.PreMeasure
            }
            wakeLock.release()
            if (camera != null) {
                camera!!.setPreviewCallback(null)
                camera!!.stopPreview()
                camera!!.release()
                camera = null
            }
        }
    }
}


enum class Mode{
    Peace, Relax;
}

enum class State{
    PreMeasure, Measure, AfterMeasure
}
