package com.example.biohub.ui.auth

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.DatePicker
import android.widget.RadioGroup
import androidx.fragment.app.Fragment
import com.example.biohub.MainActivity
import com.example.biohub.R
import com.example.biohub.core.UserManager
import com.example.serega.biometrichub.models.Token
import com.example.serega.biometrichub.models.User
import kotlinx.android.synthetic.main.item_new_account_page_birthday.*
import org.jetbrains.anko.support.v4.toast
import java.text.SimpleDateFormat
import java.util.*

class NewAccountBirthdayFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.item_new_account_page_birthday, container, false)
        val dp: DatePicker = view.findViewById(R.id.dp_birthday)
        val btnNext: Button = view.findViewById(R.id.btn_next)

        dp.init(2000, 0, 1, null)

        btnNext.setOnClickListener {
            val bundle = this.activity?.intent?.extras
            if (bundle != null && bundle.getString("authKind") == "GOOGLE") {
                val genderRadioGroup = fragmentManager!!.fragments[0].view!!.findViewById<RadioGroup>(R.id.rg_gender)  // TODO: unsafe
                val gender = if (genderRadioGroup.checkedRadioButtonId == R.id.rb_gender_m) "m" else "f"

                val calendar = Calendar.getInstance()
                calendar.set(dp_birthday.year, dp_birthday.month, dp_birthday.dayOfMonth)
                val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                val birthday = dateFormat.format(calendar.time)

                val userManager = UserManager.getInstance(this.context!!)
                userManager.registerGoogleUser(gender, birthday, bundle.getString("idToken", ""),
                    successCallback = { response ->
                        Log.e("RECEIVED AT AUTH", "$response")
                        val status = response.getString("result")
                        if (status.equals("ok")) {
                            val data = response.getJSONObject("data")
                            val name = bundle.getString("name", "Username")
                            val id = data.getLong("user_id")
                            val token = data.getString("token")
                            userManager.setUser(User(id, name, Token(token)))
                            Log.e("AT AUTH", "User set ${userManager.getUser()!!.id}")
                            val intent = Intent(this.context, MainActivity::class.java)
                            startActivity(intent)
                        }
                        else if (status.equals("error")) {
                            toast("Error: ${response.getString("error")}")
                        }
                    }, errorCallback = {
                        toast("Failed: ${it.message}")
                    })
            }
        }

        return view
    }
}